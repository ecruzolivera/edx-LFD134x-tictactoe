# EDX WebAssembly Actors: From Cloud to Edge Tic Tac Toe Exercise 

https://learning.edx.org/course/course-v1:LinuxFoundationX+LFD134x+1T2021/home

## Stuff to improve

- `get_winner` logic is to imperative
- unit tests


## To run it as a WASI binary

```sh
cargo build --target wasm32-wasi
wasmtime target/wasm32-wasi/debug/tictactoe.wasm
``` 

## Game run example

```
Tic Tac Toe
-------
| | | |
-------
| | | |
-------
| | | |
-------

X's turn, enter row, col to play or "q" to end the game:
0,0
-------
|X| | |
-------
| | | |
-------
| | | |
-------

O's turn, enter row, col to play or "q" to end the game:
1,1
-------
|X| | |
-------
| |O| |
-------
| | | |
-------

X's turn, enter row, col to play or "q" to end the game:
0,2
-------
|X| |X|
-------
| |O| |
-------
| | | |
-------

O's turn, enter row, col to play or "q" to end the game:
1,0
-------
|X| |X|
-------
|O|O| |
-------
| | | |
-------

X's turn, enter row, col to play or "q" to end the game:
0,1
-------
|X|X|X|
-------
|O|O| |
-------
| | | |
-------

X WINS!!!
```
