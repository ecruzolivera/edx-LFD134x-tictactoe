use std::{io, usize};

fn main() {
    println!("Tic Tac Toe");
    let mut tictactoe = tictactoe::GameBoard::default();
    loop {
        println!("{}", tictactoe);
        println!(
            "{}'s turn, enter row, col to play or \"q\" to end the game:",
            tictactoe.get_turn()
        );
        match process_input() {
            InputCmd::Play(r, c) => {
                if let Err(e) = tictactoe.play(r, c) {
                    eprintln!("{}", e);
                    continue;
                }
            }
            InputCmd::Quit => {
                println!("Game Ended");
                break;
            }
            InputCmd::Invalid(s) => eprintln!("{}", s),
        }
        if let Some(w) = tictactoe.get_winner() {
            println!("{}", tictactoe);
            println!("{} WINS!!!", w);
            break;
        }
    }
}

enum InputCmd {
    Play(usize, usize),
    Quit,
    Invalid(String),
}

fn process_input() -> InputCmd {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap_or_default();
    if input.is_empty() {
        return InputCmd::Invalid(String::from("Empty input"));
    }

    let coords_raw: Vec<&str> = input.trim().split(',').collect();
    if coords_raw[0].to_lowercase() == "q" {
        return InputCmd::Quit;
    } else if coords_raw.len() != 2 {
        return InputCmd::Invalid(format!("Wrong coordinates format: {}", input));
    }
    let maybe_row = coords_raw[0].parse::<usize>();
    let maybe_col = coords_raw[1].parse::<usize>();
    match (maybe_row, maybe_col) {
        (Ok(r), Ok(c)) => InputCmd::Play(r, c),
        _ => InputCmd::Invalid(format!("Wrong coordinates format: {}", input)),
    }
}
