use std::{
    fmt::{self, Display},
    usize,
};

#[derive(Debug, Clone, PartialEq, Copy)]
pub enum GamePiece {
    X,
    O,
    Empty,
}

impl Display for GamePiece {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            GamePiece::Empty => write!(f, " "),
            GamePiece::X => write!(f, "X"),
            GamePiece::O => write!(f, "O"),
        }
    }
}

pub struct GameBoard {
    board: [GamePiece; GameBoard::COLS * GameBoard::ROWS],
    turn: GamePiece,
}

impl GameBoard {
    const COLS: usize = 3;
    const ROWS: usize = 3;
    const WIN_CONDITIONS: [[usize; 3]; 8] = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    fn get_piece(&self, row: usize, col: usize) -> GamePiece {
        self.board[row * GameBoard::ROWS + col]
    }

    fn set_piece(&mut self, row: usize, col: usize, piece: GamePiece) -> Result<(), String> {
        if row >= GameBoard::ROWS {
            return Err(format!("Row: {} out of Bounds", row));
        } else if col >= GameBoard::COLS {
            return Err(format!("Col: {} out of Bounds", col));
        } else if self.board[row * GameBoard::ROWS + col] != GamePiece::Empty {
            return Err(format!("Coords: {:?} already played", (row, col)));
        } else {
            self.board[row * GameBoard::ROWS + col] = piece;
            Ok(())
        }
    }

    fn next_turn(&mut self) {
        if self.turn == GamePiece::X {
            self.turn = GamePiece::O;
        } else {
            self.turn = GamePiece::X;
        }
    }

    pub fn get_turn(&self) -> GamePiece {
        self.turn
    }

    pub fn play(&mut self, row: usize, col: usize) -> Result<(), String> {
        let piece = self.turn;
        self.set_piece(row, col, piece)?;
        self.next_turn();
        Ok(())
    }

    pub fn get_winner(&self) -> Option<GamePiece> {
        let mut winner_piece;
        let mut current = GamePiece::Empty;
        for pattern in &GameBoard::WIN_CONDITIONS {
            winner_piece = self.board[pattern[0]];
            if winner_piece == GamePiece::Empty {
                continue;
            }
            for index in &pattern[1..] {
                current = self.board[*index];
                if current != winner_piece {
                    break;
                }
            }
            if current == winner_piece {
                return Some(winner_piece);
            }
        }
        return None;
    }
}

impl Default for GameBoard {
    fn default() -> Self {
        GameBoard {
            board: [GamePiece::Empty; 9],
            turn: GamePiece::X,
        }
    }
}

impl Display for GameBoard {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for r in 0..GameBoard::ROWS {
            writeln!(f, "-------")?;
            for c in 0..GameBoard::COLS {
                write!(f, "|{}", self.get_piece(r, c))?;
            }
            writeln!(f, "|")?;
        }
        writeln!(f, "-------")?;
        Ok(())
    }
}
